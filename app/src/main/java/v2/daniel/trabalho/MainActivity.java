package v2.daniel.trabalho;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etExpressao;
    private String expressao;
    private Button btnMais;
    private Button btnMenos;
    private Button btnMult;
    private Button btnDivi;
    private Button btnEspecial;
    private Button btnCalcular;

    private Button btnParentesesE;
    private Button btnParentesesD;
    private Button btnLimpar;

    private TextView tvResultado;

    private String especialNumber;
    private List<String> list_expressao = new ArrayList<>();

    private LinearLayout layoutLoading;
    private ProgressBar pbLoading;
    private TextView tvLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.etExpressao = (EditText) findViewById(R.id.etExpressao);
        this.btnMais = (Button) findViewById(R.id.btnMais);
        this.btnMenos = (Button) findViewById(R.id.btnMenos);
        this.btnMult = (Button) findViewById(R.id.btnMult);
        this.btnDivi = (Button) findViewById(R.id.btnDivi);

        this.btnParentesesE = (Button) findViewById(R.id.btnParentesesE);
        this.btnParentesesD = (Button) findViewById(R.id.btnParentesesD);
        this.btnLimpar = (Button) findViewById(R.id.btnLimpar);

        this.btnEspecial = (Button) findViewById(R.id.btnEspecial);
        this.btnCalcular = (Button) findViewById(R.id.btnCalcular);

        this.tvResultado = (TextView) findViewById(R.id.tvResultado);

        this.layoutLoading = (LinearLayout) findViewById(R.id.layoutLoading);
        this.pbLoading = (ProgressBar) findViewById(R.id.pbLoading);
        this.tvLoading = (TextView) findViewById(R.id.tvLoading);

        textChange();

        this.btnMais.setOnClickListener(this);
        this.btnMenos.setOnClickListener(this);
        this.btnMult.setOnClickListener(this);
        this.btnDivi.setOnClickListener(this);
        this.btnCalcular.setOnClickListener(this);
        this.btnEspecial.setOnClickListener(this);
        this.btnParentesesE.setOnClickListener(this);
        this.btnParentesesD.setOnClickListener(this);
        this.btnLimpar.setOnClickListener(this);

    }

    void textChange() {
        this.etExpressao.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String ultimoCaractere = "";
                if (etExpressao.getText().toString().length() > 0) {
                    ultimoCaractere = String.valueOf(s.charAt(s.length()-1));

                    if (etExpressao.getText().toString().length() == 1) {
                        if (ultimoCaractere.equals("+") || ultimoCaractere.equals("-")
                                || ultimoCaractere.equals("x") || ultimoCaractere.equals("/")
                                || ultimoCaractere.equals("(") || ultimoCaractere.equals(")")) {
                            etExpressao.setText("");
                        }
                    }
                }

                if (ultimoCaractere.equals("+") || ultimoCaractere.equals("-")
                        || ultimoCaractere.equals("x") || ultimoCaractere.equals("/") || ultimoCaractere.equals("#")
                        || ultimoCaractere.equals("(")) {

                    if (ultimoCaractere.equals("#")) {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.laranja));
                    }else {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.vermelho));
                    }

                }else {
                    if (temParentesesInvalido(expressao)) {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.vermelho));
                    }else {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.azul));
                    }

                }

                if (!s.toString().equals("+") && !s.toString().equals("-")
                        && !s.toString().equals("x") && !s.toString().equals("/")
                        && !s.toString().equals("(") && !s.toString().equals(")")) {
                    expressao = s.toString();
                }

                if (!s.toString().equals(" ")) {
                    Toast.makeText(MainActivity.this, expressao.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == this.btnMais.getId()) {
            actionClickMais();
        }else if (v.getId() == this.btnMenos.getId()) {
            actionClickMenos();
        }else if (v.getId() == this.btnMult.getId()) {
            actionClickMult();
        }else if (v.getId() == this.btnDivi.getId()) {
            actionClickDivi();
        }else if(v.getId() == this.btnEspecial.getId()) {
            actionClickEspecial();
        }else if(v.getId() == this.btnCalcular.getId()) {

            if (!temParentesesInvalido(expressao) && !etExpressao.getText().toString().trim().equals("")) {
                runOnUiThread(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run() {
                        layoutLoading.setVisibility(View.VISIBLE);
                        tvLoading.setText("Montando expressão, aguarde...");
                    }
                });
                actionClickCalcular();
            }
        }else if(v.getId() == this.btnParentesesE.getId()) {
            actionClickPE();
        }else if(v.getId() == this.btnParentesesD.getId()) {
            actionClickPD();
        }else if(v.getId() == this.btnLimpar.getId()) {
            actionClickLimpar();
        }
    }

    private void actionClickPE() {
        runOnUiThread(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                String ultimoCarac = "";
                if (!etExpressao.getText().toString().trim().equals("")) {
                    ultimoCarac = String.valueOf(
                            etExpressao.getText().toString().charAt(etExpressao.getText().toString().length()-1));
                }

                if (ultimoCarac.equals("(") || ultimoCarac.equals(")")) {

                    if (ultimoCarac.equals("#")) {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.laranja));
                        etExpressao.setText(etExpressao.getText().toString() + "(");
                        textChange();
                    }else {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.vermelho));
                    }

                }else {
                    etExpressao.setText(etExpressao.getText().toString() + "(");
                }
            }
        });

        this.etExpressao.setSelection(this.etExpressao.getText().length());
    }

    private void actionClickPD() {
        runOnUiThread(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                String ultimoCarac = "";
                if (!etExpressao.getText().toString().trim().equals("")) {
                    ultimoCarac = String.valueOf(
                            etExpressao.getText().toString().charAt(etExpressao.getText().toString().length()-1));
                }

                if ( ultimoCarac.equals("(") || ultimoCarac.equals(")")) {

                    if (ultimoCarac.equals("#")) {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.laranja));
                        etExpressao.setText(etExpressao.getText().toString() + ")");
                        textChange();
                    }else {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.vermelho));
                    }

                }else {
                    etExpressao.setText(etExpressao.getText().toString() + ")");
                }
            }
        });

        this.etExpressao.setSelection(this.etExpressao.getText().length());
    }

    @SuppressLint("SetTextI18n")
    private void actionClickLimpar() {
        expressao = "";
        list_expressao.clear();
//        tvResultado.setText("Resultado");
        etExpressao.setText("");
    }

    void actionClickCalcular() {
        String ultimoCarac = "";
        if (!etExpressao.getText().toString().trim().equals("")) {
            ultimoCarac = String.valueOf(
                    etExpressao.getText().toString().charAt(etExpressao.getText().toString().length()-1));

            if (!ultimoCarac.equals("+") && !ultimoCarac.equals("-")
                    && !ultimoCarac.equals("x") && !ultimoCarac.equals("/")
                    && !ultimoCarac.equals("(") && !temParentesesInvalido(expressao)) {

                for (int i =0; i < expressao.length(); i++) {
                    list_expressao.add(String.valueOf(expressao.charAt(i)));
                }

                questionEspecialNumbers();
            }
        }
    }

    private void questionEspecialNumbers() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int position=0;

                while (list_expressao.contains("#")) {
                    for (int i=position; i < expressao.length(); i++) {
                        if (expressao.charAt(i) == '#') {
                            position = i;
                            dialogQuestionEspecial();
                            try {
                                Thread.sleep(9000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            list_expressao.set(position, especialNumber);
                        }
                    }
                }
                setOnResultTes();

                runOnUiThread(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run() {
                        layoutLoading.setVisibility(View.VISIBLE);
                        tvLoading.setText("Calculando expressão, aguarde...");
                    }
                });

                organizarExpressaoString();
            }
        }).start();
    }

    private void organizarExpressaoString() {
        String e = "";

        for (int i=0; i< list_expressao.size(); i++) {
            e += list_expressao.get(i);
        }

        final String result = String.valueOf(CalculadoraExpressao.evaluate(e));

        runOnUiThread(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                tvResultado.setText("RESULTADO: " + list_expressao.toString().replaceAll("\\[", "").replaceAll("]","").replaceAll(" ", "").replaceAll(",", "") + " = " + result);
                layoutLoading.setVisibility(View.GONE);
                actionClickLimpar();
            }
        });
    }

    void setOnResultTes() {
        runOnUiThread(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                tvResultado.setText("A expressão ficou assim: " + list_expressao.toString().replaceAll("\\[", "").replaceAll("]","").replaceAll(" ", "").replaceAll(",", ""));
            }
        });
    }

    @SuppressLint("SetTextI18n")
    void actionClickEspecial() {
        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.laranja));
        etExpressao.setText(etExpressao.getText().toString() + "#");
        this.etExpressao.setSelection(this.etExpressao.getText().length());
    }

    void actionClickMais() {
        runOnUiThread(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                String ultimoCarac = "";
                if (!etExpressao.getText().toString().trim().equals("")) {
                    ultimoCarac = String.valueOf(
                            etExpressao.getText().toString().charAt(etExpressao.getText().toString().length()-1));
                }

                if (ultimoCarac.equals("+") || ultimoCarac.equals("-")
                        || ultimoCarac.equals("x") || ultimoCarac.equals("/") || ultimoCarac.equals("#")) {

                    if (ultimoCarac.equals("#")) {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.laranja));
                        etExpressao.setText(etExpressao.getText().toString() + "+");
                        textChange();
                    }else {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.vermelho));
                    }

                }else {
                    etExpressao.setText(etExpressao.getText().toString() + "+");
                }
            }
        });

        this.etExpressao.setSelection(this.etExpressao.getText().length());
    }

    void actionClickMenos() {
        runOnUiThread(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                String ultimoCarac = "";
                if (!etExpressao.getText().toString().trim().equals("")) {
                    ultimoCarac = String.valueOf(
                            etExpressao.getText().toString().charAt(etExpressao.getText().toString().length()-1));
                }

                if (ultimoCarac.equals("+") || ultimoCarac.equals("-")
                        || ultimoCarac.equals("x") || ultimoCarac.equals("/") || ultimoCarac.equals("#")) {

                    if (ultimoCarac.equals("#")) {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.laranja));
                        etExpressao.setText(etExpressao.getText().toString() + "-");
                        textChange();
                    }else {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.vermelho));
                    }

                }else {
                    etExpressao.setText(etExpressao.getText().toString() + "-");
                }
            }
        });

        this.etExpressao.setSelection(this.etExpressao.getText().length());
    }

    void actionClickMult() {
        runOnUiThread(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                String ultimoCarac = "";
                if (!etExpressao.getText().toString().trim().equals("")) {
                    ultimoCarac = String.valueOf(
                            etExpressao.getText().toString().charAt(etExpressao.getText().toString().length()-1));
                }

                if (ultimoCarac.equals("+") || ultimoCarac.equals("-")
                        || ultimoCarac.equals("x") || ultimoCarac.equals("/") || ultimoCarac.equals("#")) {

                    if (ultimoCarac.equals("#")) {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.laranja));
                        etExpressao.setText(etExpressao.getText().toString() + "x");
                        textChange();
                    }else {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.vermelho));
                    }

                }else {
                    etExpressao.setText(etExpressao.getText().toString() + "x");
                }
            }
        });

        this.etExpressao.setSelection(this.etExpressao.getText().length());
    }

    void actionClickDivi() {
        runOnUiThread(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                String ultimoCarac = "";
                if (!etExpressao.getText().toString().trim().equals("")) {
                    ultimoCarac = String.valueOf(
                            etExpressao.getText().toString().charAt(etExpressao.getText().toString().length()-1));
                }

                if (ultimoCarac.equals("+") || ultimoCarac.equals("-")
                        || ultimoCarac.equals("x") || ultimoCarac.equals("/") || ultimoCarac.equals("#")) {

                    if (ultimoCarac.equals("#")) {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.laranja));
                        etExpressao.setText(etExpressao.getText().toString() + "/");
                        textChange();
                    }else {
                        etExpressao.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.vermelho));
                    }

                }else {
                    etExpressao.setText(etExpressao.getText().toString() + "/");
                }
            }
        });

        this.etExpressao.setSelection(this.etExpressao.getText().length());
    }

    void dialogQuestionEspecial() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_question_especial, null);
                dialogBuilder.setView(dialogView);

                final EditText edt = (EditText) dialogView.findViewById(R.id.etEspecialNumber);

                dialogBuilder.setTitle("Número Misterioso");
                dialogBuilder.setMessage("Digite um valor para a variável");
                dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        especialNumber = edt.getText().toString().trim();
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        });
    }

    boolean temParentesesInvalido(String expressao) {
        if (expressao == null) {
            return false;
        }

        try {
            int countA=0;
            int countB=0;

            for (int i=0; i< expressao.length(); i++) {
                if (expressao.charAt(i) == '(') {
                    countA += 1;
                }else if (expressao.charAt(i) == ')') {
                    countB += 1;
                }
            }

            if (countA != countB)
                return true;
            else
                return false;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
