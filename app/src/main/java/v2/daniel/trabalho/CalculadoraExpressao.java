package v2.daniel.trabalho;

import java.util.Stack;

/**
 * @author Daniel Monteiro
 *
 * @since on 19/11/17.
 */

public class CalculadoraExpressao {

    public static int evaluate(String expression) {
        char[] tokens = expression.toCharArray();

        // Stack para os números:
        Stack<Integer> valores = new Stack<>();

        // Stack para as operações
        Stack<Character> ops = new Stack<>();

        for (int i = 0; i < tokens.length; i++) {

            if (tokens[i] == ' ')
                continue;

            // Se o atual caractere for um número, colocar na stack de números
            if (tokens[i] >= '0' && tokens[i] <= '9') {
                StringBuffer sbuf = new StringBuffer();

                // O número pode ser mais que um dígito:
                while (i < tokens.length && tokens[i] >= '0' && tokens[i] <= '9') {
                    sbuf.append(tokens[i++]);
                }

                // volta um índice, porque no for vai incrementar mais um, e iria ignorar o caractere de operação?:
                --i;

                valores.push(Integer.parseInt(sbuf.toString()));
            }

            // Se o caractere atual é uma abertura de parênteses:
            else if (tokens[i] == '(')
                ops.push(tokens[i]);

                // Se o caractere atual é uma fechadura de parênteses:
            else if (tokens[i] == ')') {
                while (ops.peek() != '(')
                    valores.push(applyOp(ops.pop(), valores.pop(), valores.pop()));
                ops.pop();
            }

            // Se o caractere atual é um operador:
            else if (tokens[i] == '+' || tokens[i] == '-' ||
                    tokens[i] == 'x' || tokens[i] == '/') {

                // Aplicar a operação para os dois últimos elementos da satck de valores:
                while (!ops.empty() && hasPrecedence(tokens[i], ops.peek()))
                    valores.push(applyOp(ops.pop(), valores.pop(), valores.pop()));

                // Colocar o caractere atual(alguma operação), na satck de operações
                ops.push(tokens[i]);
            }
        }

        // Toda a expressão já foi analisada
        // Realiza as operações restantes:
        while (!ops.empty())
            valores.push(applyOp(ops.pop(), valores.pop(), valores.pop()));

        // O top da stack values tem o resultado
        return valores.pop();
    }

    // Retorna verdadeiro se 'op2' tiver precedência maior ou igual a 'op1',
    // de outra forma, retorna falso.
    public static boolean hasPrecedence(char op1, char op2) {
        if (op2 == '(' || op2 == ')')
            return false;
        if ((op1 == 'x' || op1 == '/') && (op2 == '+' || op2 == '-'))
            return false;
        else
            return true;
    }

    // Aplica o cálculo de acordo com a operação passada:
    public static int applyOp(char op, int b, int a) {
        switch (op)
        {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case 'x':
                return a * b;
            case '/':
                if (b == 0)
                    throw new
                            UnsupportedOperationException("Cannot divide by zero");
                return a / b;
        }
        return 0;
    }
}
